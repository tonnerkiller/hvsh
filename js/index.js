// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

//toggle popover
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
  // when closing delete svg
  $("#myModal").on("hide.bs.modal", function () {
    d3.selectAll('svg').remove();
  });
});

// helper functions for D3
function hrfunction(m, b, index, step, amount){
  let array = [];
  let ypsilon;
  for (let i = index; i <= index+amount; i = i + step){
    ypsilon = m*i+parseFloat(b);
    array.push({x: i, y: ypsilon});
  };
  return array;
};

// extreme values
function extremes(data){
  let minx = data[0].x;
  let maxx = data[0].x;
  let miny = data[0].y;
  let maxy = data[0].y;

  for (let i=1; i<data.length; i++){
    if (data[i].x>maxx){
      maxx = data[i].x;
    }else{
      minx = data[i].x;
    }

    if (data[i].y>maxy){
      maxy = data[i].y;
    }else{
      miny = data[i].y;
    }
  }
  return {minx: minx, maxx:maxx, miny: miny, maxy: maxy}
}

// svg reset

function resetSVG(){

}

// form computation
function processForm() {

  // first of all setup event listeners for cleaning up


  let hrgrundg = document.forms["calcForm"]["hrgrundg"].value;
  let hrprovision  = document.forms["calcForm"]["hrprovision"].value;
  let hvprovision = document.forms["calcForm"]["hvprovision"].value;

  let titel = "";
  let desc = "";

  let diff = hvprovision - hrprovision

  if (diff == 0){
    titel = "Lustig! Aber denken wir den Spaß zu Ende:";
    if (hrgrundg < 0){
      desc = "Der Handlungsreisende erhält ein <emph>negatives</emph> Grundgehalt. Er bringt also quasi Geld zur Arbeit. Weil er außerdem die gleiche Provision wie der Handelsvertreter erhält schneiden sich beide Kurven nie und er wird immer die für den Betrieb günstigere Option sein.";
      // hier ein graph
    }else if(hrgrundg > 0){
      desc = "Der Handlungsreisende erhält ein <emph>positives</emph> Grundgehalt. Da er die gleiche Provision wie der Handelsvertreter erhält, schneiden sich beide Kurven nie und er wird immer die für den Betrieb teurere Option sein.";
    }else{
      desc = "Handlungsreisender und Handelsvertreter erhalten die gleiche Provision. Da außerdem der Handlungsreisende wie der Handelsvertreter kein Grundgehalt erhält, sind vom finanziellen Standpunkt her beide für den Betrieb identisch zu bewerten. Allerdings ist der Handlungsreisende angestellt. Sollte es auf dem Arbeitsmarkt eine Knappheit in diesem Berufsfeld geben, wird sich der Betrieb entscheiden, jemanden fest an sich zu binden. Gibt es ein Überangebot in diesem Berufsfeld, wird der Betrieb erpicht sein, flexibel zu bleiben und lieber Aufträge an einen Handelsvertreter geben als einen Handlungsreisenden einzustellen."
    }
  }else{
    let umsatz =  hrgrundg / diff;

    // Unsinn
    if ((hrgrundg<=0)||(hrprovision<0)||(hvprovision<=0)||(hvprovision<hrprovision)){
      titel = "Lustig! Aber denken wir den Spaß zu Ende:";
      desc = "Gleiche Kosten entstehen bei einem Umsatz von "+umsatz.toFixed(2)+".\n";
    }else{
      titel = "Gleiche Kosten entstehen bei einem Umsatz von "+umsatz.toFixed(2);
    }

    // wer je günstiger ist hängt ab vom Umsatz und dem Grundgehalt des Handlungsreisenden
    // haben beide das gleiche Vorzeichen, ist vor der Kostengleichheit (bei niedrigerem Umsatz) der Vertreter günstiger.
    // haben beide verschiedene Vorzeichen, ist bei niedrigerem Umsatz als der Kostengleichheit der Handlungsreisende günstiger

    if ((( umsatz > 0 ) && ( hrgrundg < 0 )) || (( umsatz < 0 ) && ( hrgrundg > 0 ))) {
      // verschiedene Vorzeichen
      desc += "Ist der Umsatz niedriger, ist der Handlungsreisende günstiger. Ist der Umsatz höher, ist der Handelsvertreter günstiger.";
    }else{
      // gleiche Vorzeichen
      desc += "Ist der Umsatz niedriger, ist der Handelsvertreter günstiger. Ist der Umsatz höher, ist der Handlungsreisende günstiger.";
    }

    // draw the graph

    let width = 500;
    let height = 500;


    //let mod = d3.select('myModal').attr('width', width);

    let vis = d3.select("#graph").append("svg");
    vis.attr("width", width)
       .attr("height", height)
       .style("border", "1px solid black");

    let hrnodes = hrfunction(hrprovision, hrgrundg, -2*umsatz, umsatz, 4*umsatz);
    let hvnodes = hrfunction(hvprovision, 0, -2*umsatz, umsatz, 4*umsatz);



    //let extv = extremes(hrnodes.concat(hvnodes));
    let data = hrnodes.concat(hvnodes);
//    let scale = scaleLinear()
  //  .domain([extv.minx, extv.maxx])   // x-Achse
  //  .range([extv.miny, extv.maxx]);   //y- Achse

  let x = d3.scaleLinear()
      .domain([d3.min(data, function(d) { return d.x; }), d3.max(data, function(d) { return d.x; })])
      .range([ 0, width ]);
  vis.append('g')
  .attr("transform", "translate(0, "+1/2*height+")")
  .call(d3.axisBottom(x));

    let y = d3.scaleLinear()
        .domain([d3.min(data, function(d) { return d.y; }), d3.max(data, function(d) { return d.y; })])
        .range([ height, 0 ]);
    vis.append("g")
        .attr("transform", "translate("+1/2*width+", 0)")
        .call(d3.axisRight(y));

        let lineFunc = d3.line()
        .x(function(d){return x(d.x)})
        .y(function(d){return y(d.y)});

    vis.append('path')
    .attr('d', lineFunc(hrnodes))
    .attr('stroke', 'black')
    .attr('fill', 'none')
    .text('Handlungsreisender');

    vis.append('path')
    .attr('d', lineFunc(hvnodes))
    .attr('stroke', 'blue')
    .attr('fill', 'none');

//console.log(hrnodes);

//console.log(hvnodes);




  //  vis.selectAll("circle .hrnodes")
  //       .data(hrnodes)
  //       .enter()
  //       .append("svg:circle")
  //       .attr("class", "hrnodes")
  //       .attr("cx", function(d) { return d.x; })
  //       .attr("cy", function(d) { return d.y; })
  //       .attr("r", "1px")
  //       .attr("fill", "black") ;

  }

  //alert("Ab einem Umsatz von "+umsatz.toFixed(2)+" ist der Handlungsreisende günstiger als der Handelsvertreter");
  //let title =
  document.getElementsByClassName("modal-title")[0].innerHTML = titel;
  document.getElementsByClassName("modal-body")[0].innerHTML = desc;
  //title[0].innerHTML="Das Ergebnis lautet:";
  //alert(title[0]);
  $("#myModal").modal()
  return false;
}
